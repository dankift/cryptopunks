#!/bin/bash -e

# Set defaults
MEMORY_LIMIT=${MEMORY_LIMIT:-'512m'}
SPRING_PROFILES_ACTIVE=${SPRING_PROFILES_ACTIVE:-'default'}

function launchApp {
  javaParams=''

  # listen on all the interfaces
  javaParams+=" -Dserver.address=0.0.0.0"

  # set SPRING profile
  javaParams+=" -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE"

  echo "COMMAND: java ${javaParams} -Dfile.encoding=UTF-8 -Xmx${MEMORY_LIMIT} -jar"
  java ${javaParams} -Dfile.encoding=UTF-8 -Xmx${MEMORY_LIMIT} -jar $JAR_FILE
}

launchApp
