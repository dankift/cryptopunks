package com.dankift.cryptopunks

import org.springframework.boot.builder.SpringApplicationBuilder

/*
 * Allows starting the application with test containers running
 */
class StartApplication {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplicationBuilder(CryptoPunksApplication::class.java)
                .profiles("integration-test")
                .initializers(Initializer())
                .run(*args)
        }
    }
}
