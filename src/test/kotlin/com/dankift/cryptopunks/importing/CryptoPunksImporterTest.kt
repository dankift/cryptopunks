package com.dankift.cryptopunks.importing

import com.dankift.cryptopunks.IntegrationTestParent
import com.dankift.cryptopunks.model.CryptoPunkGender
import com.dankift.cryptopunks.model.CryptoPunksRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class CryptoPunksImporterTest(
    @Autowired private val cryptoPunksRepository: CryptoPunksRepository
) : IntegrationTestParent() {

    private lateinit var cryptoPunksImporter: CryptoPunksImporter

    @BeforeEach
    fun setUp() {
        cryptoPunksImporter = CryptoPunksImporter(
            fileName = "CryptoPunksTest.json",
            cryptoPunksRepository = cryptoPunksRepository
        )
    }

    @AfterEach
    fun tearDown() {
        cryptoPunksRepository.deleteAllInBatch()
    }

    @Test
    fun `should import cryptopunks and their attributes from json file`() {
        cryptoPunksImporter.import()

        assertThat(cryptoPunksRepository.findAll()).hasSize(5)

        val punk = cryptoPunksRepository.getOne(1L)
        assertThat(punk.id).isEqualTo(1)
        assertThat(punk.forSale).isEqualTo(false)
        assertThat(punk.gender).isEqualTo(CryptoPunkGender.MALE)
        assertThat(punk.accessories.map { it.name }).containsExactlyInAnyOrder("Smile", "Mohawk")
    }
}
