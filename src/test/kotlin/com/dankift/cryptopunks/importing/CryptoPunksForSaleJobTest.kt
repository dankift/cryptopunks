package com.dankift.cryptopunks.importing

import com.dankift.cryptopunks.IntegrationTestParent
import com.dankift.cryptopunks.model.CryptoPunk
import com.dankift.cryptopunks.model.CryptoPunkGender
import com.dankift.cryptopunks.model.CryptoPunksRepository
import com.ninjasquad.springmockk.MockkBean
import io.micrometer.core.instrument.MeterRegistry
import io.mockk.coEvery
import java.math.BigDecimal
import java.math.BigInteger
import java.time.Duration
import java.util.UUID
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.test.context.TestPropertySource

@TestPropertySource(properties = ["job.end-index=9", "job.batch-size=5"])
@ExperimentalCoroutinesApi
class CryptoPunksForSaleJobTest(
    @Autowired private val jobProperties: JobProperties,
    @Autowired private val meterRegistry: MeterRegistry,
    @Autowired private val applicationEventPublisher: ApplicationEventPublisher,
    @Autowired private var cryptoPunksRepository: CryptoPunksRepository
) : IntegrationTestParent() {

    @MockkBean
    private lateinit var cryptoPunksClient: CryptoPunksClient

    private lateinit var cryptoPunksForSaleJob: CryptoPunksForSaleJob

    @BeforeEach
    fun setUp() {
        cryptoPunksForSaleJob =
            CryptoPunksForSaleJob(jobProperties, cryptoPunksClient, applicationEventPublisher, meterRegistry)
    }

    @AfterEach
    fun tearDown() {
        cryptoPunksRepository.deleteAllInBatch()
    }

    @Test
    fun `should check for for sale status for each punk and update database`() {
        cryptoPunksRepository.saveAll((0..9L).map {
            CryptoPunk(
                id = it,
                forSale = false,
                gender = CryptoPunkGender.APE
            )
        })
        coEvery { cryptoPunksClient.getPunkOffer(any()) } answers {
            offer(
                punkIndex = firstArg(),
                isForSale = firstArg<BigInteger>() == BigInteger.valueOf(7)
            )
        }

        cryptoPunksForSaleJob.getPunkOffers()

        await().atMost(Duration.ofSeconds(60)).untilAsserted {
            assertThat(meterRegistry.counter("offers.job.success").count()).isEqualTo(1.0)
        }
        assertThat(cryptoPunksRepository.getOne(7L).forSale).isTrue()
        assertThat(cryptoPunksRepository.findAll().filter { it.forSale }).hasSize(1)
    }

    private fun offer(
        isForSale: Boolean,
        punkIndex: BigInteger,
        onlySellTo: String = "",
        seller: String = UUID.randomUUID().toString(),
        minValue: BigDecimal = BigDecimal.TEN
    ): Offer {
        return Offer(
            isForSale = isForSale,
            punkIndex = punkIndex,
            onlySellTo = onlySellTo,
            minValue = minValue,
            seller = seller
        )
    }
}
