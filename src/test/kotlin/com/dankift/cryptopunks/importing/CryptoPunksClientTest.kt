package com.dankift.cryptopunks.importing

import com.dankift.cryptopunks.IntegrationTestParent
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.mockk
import java.math.BigDecimal
import java.math.BigInteger
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.web3j.model.CryptoPunksMarket
import org.web3j.protocol.core.RemoteCall
import org.web3j.tuples.generated.Tuple5

class CryptoPunksClientTest(
    @Autowired private val cryptoPunksClient: CryptoPunksClient
) : IntegrationTestParent() {

    @MockkBean
    private lateinit var cryptoPunksMarket: CryptoPunksMarket

    private var mockRemoteCall: RemoteCall<Tuple5<Boolean, BigInteger, String, BigInteger, String>> = mockk()

    @Test
    fun `should call punksOfferedForSale function on smart contract and convert response to offer object`() {
        val punkId = 1L
        every { cryptoPunksMarket.punksOfferedForSale(any()) } returns mockRemoteCall
        every { mockRemoteCall.send() } returns Tuple5(
            true,
            BigInteger.valueOf(punkId),
            "seller address",
            BigInteger.valueOf(565000000000000000),
            ""
        )

        val result = runBlocking { cryptoPunksClient.getPunkOffer(BigInteger.valueOf(punkId)) }

        assertThat(result).isEqualTo(
            Offer(
                isForSale = true,
                punkIndex = BigInteger.valueOf(punkId),
                seller = "seller address",
                minValue = BigDecimal.valueOf(0.565),
                onlySellTo = ""
            )
        )
    }
}
