package com.dankift.cryptopunks

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.restassured.RestAssured.config
import io.restassured.builder.RequestSpecBuilder
import io.restassured.config.ObjectMapperConfig.objectMapperConfig
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.restdocs.RestDocumentationContextProvider
import org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint
import org.springframework.restdocs.operation.preprocess.Preprocessors.removeHeaders
import org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.documentationConfiguration
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.testcontainers.containers.GenericContainer

private class KGenericContainer(imageName: String) : GenericContainer<KGenericContainer>(imageName)

@ContextConfiguration(initializers = [Initializer::class])
@ActiveProfiles("integration-test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract class IntegrationTestParent {

    @LocalServerPort
    private val port: Int = 0

    fun configureRestAssured(restDocumentation: RestDocumentationContextProvider): RequestSpecBuilder {
        return RequestSpecBuilder()
            .setConfig(
                config().objectMapperConfig(
                    objectMapperConfig().jackson2ObjectMapperFactory { _, _ ->
                        jacksonObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    }
                ))
            .addFilter(
                documentationConfiguration(restDocumentation)
                    .operationPreprocessors()
                    .withRequestDefaults(removeHeaders("Host", "Content-Length"))
                    .withResponseDefaults(prettyPrint())
            )
            .setBaseUri("http://localhost")
            .setPort(port)
    }
}

/*
 * Starts test container for database, and configures application properties to use the container host and port
 */
class Initializer : ApplicationContextInitializer<ConfigurableApplicationContext> {

    override fun initialize(configurableApplicationContext: ConfigurableApplicationContext) {

        val values = TestPropertyValues.of(
            "spring.datasource.url=jdbc:postgresql://${postgres.containerIpAddress}:${postgres.firstMappedPort}/$databaseName",
            "spring.datasource.username=$databaseUsername",
            "spring.datasource.password=$databasePassword"
        )

        values.applyTo(configurableApplicationContext)
    }

    companion object {
        private val postgres = KGenericContainer("postgres:10")
        private const val databaseName = "cryptopunks"
        private const val databaseUsername = "cryptopunk"
        private const val databasePassword = "secret!"

        init {
            with(postgres) {
                withExposedPorts(5432)
                withEnv("POSTGRES_DB", databaseName)
                withEnv("POSTGRES_USER", databaseUsername)
                withEnv("POSTGRES_PASSWORD", databasePassword)
            }
            postgres.start()
            println("postgres started on port ${postgres.getMappedPort(5432)}")
        }
    }
}
