package com.dankift.cryptopunks

import com.dankift.cryptopunks.model.Accessory
import com.dankift.cryptopunks.model.CryptoPunk
import com.dankift.cryptopunks.model.CryptoPunkGender
import com.dankift.cryptopunks.model.CryptoPunksRepository
import io.restassured.RestAssured.given
import io.restassured.specification.RequestSpecification
import java.math.BigDecimal
import org.apache.http.HttpStatus
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers.hasSize
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.restdocs.RestDocumentationContextProvider
import org.springframework.restdocs.RestDocumentationExtension
import org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document

@ExtendWith(RestDocumentationExtension::class)
class GetPunksTest(
    @Autowired private val cryptoPunksRepository: CryptoPunksRepository
) : IntegrationTestParent() {

    private lateinit var requestSpec: RequestSpecification

    @BeforeEach
    fun setUp(restDocumentation: RestDocumentationContextProvider) {
        requestSpec = configureRestAssured(restDocumentation)
            .setAccept("application/hal+json")
            .build()
    }

    @AfterEach
    fun tearDown() {
        cryptoPunksRepository.deleteAllInBatch()
    }

    @Test
    fun `should retrieve collection of punks`() {
        cryptoPunksRepository.saveAll(
            (1..4L).map { cryptoPunkWithId(it) }
        )

        given()
            .spec(requestSpec)
            .queryParam("page", 0)
            .queryParam("size", 3)
            .filter(document("get-crypto-punks"))
            .`when`()
            .get("/api/v1/cryptopunks")
            .then()
            .log().ifValidationFails()
            .statusCode(HttpStatus.SC_OK)
            .body("_embedded.cryptoPunkList", hasSize<Int>(3))
            .body("page.size", equalTo(3))
            .body("page.totalElements", equalTo(4))
            .body("page.totalPages", equalTo(2))
            .body("page.number", equalTo(0))
    }

    @Test
    fun `should default to page of twenty elements when no paging parameters are passed in`() {
        cryptoPunksRepository.saveAll(
            (1..50L).map { cryptoPunkWithId(it) }
        )

        given()
            .spec(requestSpec)
            .`when`()
            .get("/api/v1/cryptopunks")
            .then()
            .log().ifValidationFails()
            .statusCode(HttpStatus.SC_OK)
            .body("_embedded.cryptoPunkList", hasSize<Int>(20))
            .body("page.size", equalTo(20))
            .body("page.totalElements", equalTo(50))
            .body("page.totalPages", equalTo(3))
            .body("page.number", equalTo(0))
    }

    @Test
    fun `should allow sorting`() {
        cryptoPunksRepository.saveAll(
            (1..3L).map { cryptoPunkWithId(it) }
        )

        given()
            .spec(requestSpec)
            .queryParam("page", 0)
            .queryParam("size", 3)
            .queryParam("sort", "id,desc")
            .`when`()
            .get("/api/v1/cryptopunks")
            .then()
            .log().ifValidationFails()
            .statusCode(HttpStatus.SC_OK)
            .body("_embedded.cryptoPunkList", hasSize<Int>(3))
            .body("_embedded.cryptoPunkList[0].id", equalTo(3))
            .body("_embedded.cryptoPunkList[1].id", equalTo(2))
            .body("_embedded.cryptoPunkList[2].id", equalTo(1))
    }

    @Test
    fun `should not include punks that are not for sale`() {
        cryptoPunksRepository.saveAll(
            (1..3L).map { cryptoPunkWithId(it, it == 1L) }
        )

        given()
            .spec(requestSpec)
            .queryParam("page", 0)
            .queryParam("size", 3)
            .queryParam("sort", "id,desc")
            .`when`()
            .get("/api/v1/cryptopunks")
            .then()
            .log().ifValidationFails()
            .statusCode(HttpStatus.SC_OK)
            .body("_embedded.cryptoPunkList", hasSize<Int>(1))
            .body("_embedded.cryptoPunkList[0].forSale", equalTo(true))
            .body("_embedded.cryptoPunkList[0].id", equalTo(1))
    }

    @Test
    fun `should return bad request for page size greater than 50`() {
        given()
            .spec(requestSpec)
            .queryParam("page", 0)
            .queryParam("size", 51)
            .`when`()
            .get("/api/v1/cryptopunks")
            .then()
            .log().ifValidationFails()
            .statusCode(HttpStatus.SC_BAD_REQUEST)
            .body("message", equalTo("Page size should be less than or equal to 50, was 51"))
    }

    private fun cryptoPunkWithId(it: Long, forSale: Boolean = true): CryptoPunk {
        return CryptoPunk(
            id = it,
            accessories = setOf(
                Accessory(
                    id = 1,
                    name = "Tassle Hat"
                )
            ),
            forSale = forSale,
            forSalePriceEther = BigDecimal(0.5),
            gender = CryptoPunkGender.values()[it.toInt() % CryptoPunkGender.values().size]
        )
    }
}
