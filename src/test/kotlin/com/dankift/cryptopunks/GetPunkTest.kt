package com.dankift.cryptopunks

import com.dankift.cryptopunks.model.Accessory
import com.dankift.cryptopunks.model.CryptoPunk
import com.dankift.cryptopunks.model.CryptoPunkGender
import com.dankift.cryptopunks.model.CryptoPunksRepository
import io.restassured.RestAssured.given
import io.restassured.specification.RequestSpecification
import java.math.BigDecimal
import org.apache.http.HttpStatus
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.restdocs.RestDocumentationContextProvider
import org.springframework.restdocs.RestDocumentationExtension
import org.springframework.restdocs.restassured3.RestAssuredRestDocumentation

@ExtendWith(RestDocumentationExtension::class)
class GetPunkTest(
    @Autowired private val cryptoPunksRepository: CryptoPunksRepository
) : IntegrationTestParent() {

    private lateinit var requestSpec: RequestSpecification

    @BeforeEach
    fun setUp(restDocumentation: RestDocumentationContextProvider) {
        requestSpec = configureRestAssured(restDocumentation)
            .setAccept("application/hal+json")
            .build()
    }

    @AfterEach
    fun tearDown() {
        cryptoPunksRepository.deleteAllInBatch()
    }

    @Test
    fun `should retrieve punk`() {
        val storedPunk = cryptoPunksRepository.save(
            CryptoPunk(
                id = 1,
                accessories = setOf(Accessory(name = "Wild Hair"), Accessory(name = "Nerd Glasses")),
                forSale = true,
                forSalePriceEther = BigDecimal(0.56),
                gender = CryptoPunkGender.MALE
            )
        )

        val returnedPunk =
            given()
                .spec(requestSpec)
                .filter(RestAssuredRestDocumentation.document("get-crypto-punk"))
                .`when`()
                .get("/api/v1/cryptopunks/{id}", 1)
                .then()
                .log().ifValidationFails()
                .statusCode(HttpStatus.SC_OK)
                .extract().body().`as`(CryptoPunk::class.java)

        assertThat(returnedPunk.id).isEqualTo(storedPunk.id)
    }

    @Test
    fun `should return not found when id is not known`() {
        given()
            .spec(requestSpec)
            .`when`()
            .get("/api/v1/cryptopunks/{id}", 1)
            .then()
            .log().ifValidationFails()
            .statusCode(HttpStatus.SC_NOT_FOUND)
            .body("message", equalTo("Punk with id 1 cannot be found"))
    }

    @Test
    fun `should return bad request when submitted id is less than 0`() {
        given()
            .spec(requestSpec)
            .`when`()
            .get("/api/v1/cryptopunks/{id}", -1)
            .then()
            .log().ifValidationFails()
            .statusCode(HttpStatus.SC_BAD_REQUEST)
            .body("message", equalTo("Id should be between 0 and 9999, was -1"))
    }

    @Test
    fun `should return bad request when submitted id is greater than 9999`() {
        given()
            .spec(requestSpec)
            .`when`()
            .get("/api/v1/cryptopunks/{id}", 10000)
            .then()
            .log().ifValidationFails()
            .statusCode(HttpStatus.SC_BAD_REQUEST)
            .body("message", equalTo("Id should be between 0 and 9999, was 10000"))
    }
}
