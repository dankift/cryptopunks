package com.dankift.cryptopunks

import io.restassured.RestAssured.given
import io.restassured.specification.RequestSpecification
import org.apache.http.HttpStatus
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.restdocs.RestDocumentationContextProvider
import org.springframework.restdocs.RestDocumentationExtension
import org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class, RestDocumentationExtension::class)
class ActuatorTest : IntegrationTestParent() {

    private lateinit var requestSpec: RequestSpecification

    @BeforeEach
    fun setUp(restDocumentation: RestDocumentationContextProvider) {
        requestSpec = configureRestAssured(restDocumentation).build()
    }

    @Test
    fun `should allow checking health of application`() {
        given()
            .spec(requestSpec)
            .filter(document("health-check"))
            .`when`()
            .get("/actuator/health")
            .then()
            .log().ifValidationFails()
            .statusCode(HttpStatus.SC_OK)
            .body("status", equalTo("UP"))
    }

    @Test
    fun `should allow checking metrics of application`() {
        given()
            .spec(requestSpec)
            .filter(document("metrics"))
            .`when`()
            .get("/actuator/metrics")
            .then()
            .log().ifValidationFails()
            .statusCode(HttpStatus.SC_OK)
    }

    @Test
    fun `should not expose actuator endpoints not listed in application yml`() {
        given()
            .spec(requestSpec)
            .`when`()
            .get("/actuator/info")
            .then()
            .log().ifValidationFails()
            .statusCode(HttpStatus.SC_NOT_FOUND)
    }
}
