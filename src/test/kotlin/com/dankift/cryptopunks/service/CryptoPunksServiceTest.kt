package com.dankift.cryptopunks.service

import com.dankift.cryptopunks.model.CryptoPunk
import com.dankift.cryptopunks.model.CryptoPunkGender
import com.dankift.cryptopunks.model.CryptoPunksRepository
import com.dankift.cryptopunks.service.CryptoPunkException.NotFoundException
import com.dankift.cryptopunks.service.CryptoPunkException.ValidationException
import io.mockk.Called
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import java.math.BigDecimal
import java.util.Optional
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest

class CryptoPunksServiceTest {

    private val cryptoPunksRepository = mockk<CryptoPunksRepository>()
    private val validationProperties = ValidationProperties(50, 0, 9_999)
    private val requestValidator = RequestValidator(validationProperties)

    private val cryptoPunksService = CryptoPunksService(cryptoPunksRepository, requestValidator)

    @Test
    fun `should throw NotFoundException if punk cannot be found`() {
        val id = 1L
        every { cryptoPunksRepository.findById(id) } returns Optional.empty()

        val exception = assertThrows<NotFoundException> {
            runBlocking { cryptoPunksService.getPunk(id) }
        }

        assertThat(exception.message).isEqualTo("Punk with id $id cannot be found")
    }

    @Test
    fun `should throw ValidationException if id is less than 0`() {
        val id = -1L

        val exception = assertThrows<ValidationException> {
            runBlocking { cryptoPunksService.getPunk(id) }
        }

        assertThat(exception.message).isEqualTo("Id should be between 0 and 9999, was -1")
        verify { cryptoPunksRepository.findById(id) wasNot Called }
    }

    @Test
    fun `should throw ValidationException if id is greater than 9999`() {
        val id = 10000L

        val exception = assertThrows<ValidationException> {
            runBlocking { cryptoPunksService.getPunk(id) }
        }

        assertThat(exception.message).isEqualTo("Id should be between 0 and 9999, was 10000")
        verify { cryptoPunksRepository.findById(id) wasNot Called }
    }

    @Test
    fun `should return punk from repository if request is valid`() {
        val id = 1L
        val expected = punkWithId(id)
        every { cryptoPunksRepository.findById(id) } returns Optional.of(expected)

        val actual = runBlocking { cryptoPunksService.getPunk(id) }

        assertThat(expected).isEqualTo(actual)
        verify { cryptoPunksRepository.findById(id) }
    }

    @Test
    fun `should throw ValidationException if requested page is too large`() {
        val pageable = PageRequest.of(0, 100)

        val exception = assertThrows<ValidationException> {
            runBlocking { cryptoPunksService.getPunksForSale(pageable) }
        }

        assertThat(exception.message).isEqualTo("Page size should be less than or equal to 50, was 100")
        verify { cryptoPunksRepository.findAll(pageable) wasNot Called }
    }

    @Test
    fun `should return punks from repository if request is valid`() {
        val pageRequest = PageRequest.of(0, 10)
        every { cryptoPunksRepository.findCryptoPunksByForSaleIsTrue(pageRequest) } returns PageImpl(
            listOf(
                punkWithId(1),
                punkWithId(2)
            )
        )

        val actual = runBlocking { cryptoPunksService.getPunksForSale(pageRequest) }

        assertThat(actual.content).hasSize(2)
        verify { cryptoPunksRepository.findCryptoPunksByForSaleIsTrue(pageRequest) }
    }

    private fun punkWithId(id: Long): CryptoPunk {
        return CryptoPunk(
            id = id,
            gender = CryptoPunkGender.ALIEN,
            forSale = true,
            forSalePriceEther = BigDecimal.valueOf(0.5),
            accessories = emptySet()
        )
    }
}
