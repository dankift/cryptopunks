drop table if exists crypto_punk;
create table crypto_punk
(
    id bigint not null primary key,
    gender text not null,
    for_sale boolean not null default false,
    for_sale_price_ether decimal
);

create index crypto_punk_for_sale_idx on crypto_punk(for_sale);

create sequence accessory_id_seq;

drop table if exists accessory;
create table accessory
(
    id serial primary key,
    name text not null
);

drop table if exists cryptopunk_accessories;
create table cryptopunk_accessories
(
    crypto_punk_id bigint references crypto_punk(id),
    accessory_id bigint references accessory(id),
    PRIMARY KEY (crypto_punk_id, accessory_id)
);
