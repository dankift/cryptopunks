package com.dankift.cryptopunks.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany

/*
 * Doesn't use data classes with val properties because JPA is not designed to work
 * with immutable classes or the methods generated automatically by data classes
 */
@Entity
class CryptoPunk(

    @Id
    var id: Long = 0,

    var forSale: Boolean = false,

    var forSalePriceEther: BigDecimal = BigDecimal.ZERO,

    @Enumerated(EnumType.STRING)
    var gender: CryptoPunkGender = CryptoPunkGender.ALIEN,

    @ManyToMany(
        cascade = [CascadeType.ALL],
        fetch = FetchType.EAGER
    )
    @JoinTable(
        name = "cryptopunk_accessories",
        joinColumns = [JoinColumn(name = "crypto_punk_id")],
        inverseJoinColumns = [JoinColumn(name = "accessory_id")]
    )
    var accessories: Set<Accessory> = mutableSetOf()

)

enum class CryptoPunkGender {

    @JsonProperty("male")
    MALE,

    @JsonProperty("female")
    FEMALE,

    @JsonProperty("alien")
    ALIEN,

    @JsonProperty("zombie")
    ZOMBIE,

    @JsonProperty("ape")
    APE
}
