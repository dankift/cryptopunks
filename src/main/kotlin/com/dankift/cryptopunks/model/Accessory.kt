package com.dankift.cryptopunks.model

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToMany
import javax.persistence.SequenceGenerator

@Entity
class Accessory(

    @Id
    @SequenceGenerator(name = "accessory_id_seq", sequenceName = "accessory_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "accessory_id_seq")
    var id: Long = 0,

    var name: String = "",

    @JsonIgnore
    @ManyToMany(mappedBy = "accessories")
    var cryptoPunk: Set<CryptoPunk> = emptySet()

)
