package com.dankift.cryptopunks.model

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CryptoPunksRepository : JpaRepository<CryptoPunk, Long> {

    fun findCryptoPunksByForSaleIsTrue(pageable: Pageable): Page<CryptoPunk>
}
