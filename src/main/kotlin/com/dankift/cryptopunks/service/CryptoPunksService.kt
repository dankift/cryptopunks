package com.dankift.cryptopunks.service

import com.dankift.cryptopunks.model.CryptoPunk
import com.dankift.cryptopunks.model.CryptoPunksRepository
import com.dankift.cryptopunks.service.CryptoPunkException.NotFoundException
import com.dankift.cryptopunks.service.CryptoPunkException.ValidationException
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class CryptoPunksService(
    private val cryptoPunksRepository: CryptoPunksRepository,
    private val requestValidator: RequestValidator
) {

    suspend fun getPunk(id: Long): CryptoPunk {
        val errors = requestValidator.validatePunkRequest(id)
        if (errors != null) {
            throw ValidationException(errors.getErrors())
        }
        return cryptoPunksRepository
            .findById(id)
            .orElseThrow {
                NotFoundException("Punk with id $id cannot be found")
            }
    }

    suspend fun getPunksForSale(pageable: Pageable): Page<CryptoPunk> {
        val errors = requestValidator.validatePunksRequest(pageable)
        if (errors != null) {
            throw ValidationException(errors.getErrors())
        }
        return cryptoPunksRepository.findCryptoPunksByForSaleIsTrue(pageable)
    }
}
