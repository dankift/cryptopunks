package com.dankift.cryptopunks.service

import org.springframework.http.HttpStatus

sealed class CryptoPunkException(message: String) : RuntimeException(message) {

    abstract fun status(): HttpStatus
    abstract fun message(): String

    class NotFoundException(message: String) : CryptoPunkException(message) {
        override fun status() = HttpStatus.NOT_FOUND
        override fun message(): String = message ?: "Not Found"
    }

    class ValidationException(message: String) : CryptoPunkException(message) {
        override fun status() = HttpStatus.BAD_REQUEST
        override fun message(): String = message ?: "Bad Request"
    }
}
