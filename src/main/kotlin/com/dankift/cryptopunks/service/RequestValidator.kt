package com.dankift.cryptopunks.service

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component

@Component
class RequestValidator(private val validationProperties: ValidationProperties) {

    fun validatePunkRequest(id: Long): Errors? =
        if (id < validationProperties.minValidId || id > validationProperties.maxValidId) {
            Errors().addError("Id should be between ${validationProperties.minValidId} and ${validationProperties.maxValidId}, was $id")
        } else {
            null
        }

    fun validatePunksRequest(pageable: Pageable): Errors? =
        if (pageable.pageSize > validationProperties.maxPageSize) {
            Errors().addError("Page size should be less than or equal to ${validationProperties.maxPageSize}, was ${pageable.pageSize}")
        } else {
            null
        }

    inner class Errors {
        private val errors: MutableList<String> = mutableListOf()

        fun addError(error: String): Errors {
            errors.add(error)
            return this
        }

        fun getErrors(): String = errors.joinToString()
    }
}

@ConfigurationProperties("validation")
data class ValidationProperties(
    val maxPageSize: Int,
    val minValidId: Int,
    val maxValidId: Int
)
