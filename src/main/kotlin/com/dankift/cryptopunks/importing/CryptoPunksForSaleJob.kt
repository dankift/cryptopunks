package com.dankift.cryptopunks.importing

import io.micrometer.core.instrument.MeterRegistry
import java.math.BigInteger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.supervisorScope
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/*
 * Periodically checks the for sale status of punks and publishes results as OfferRetrievedEvents
 */
@ExperimentalCoroutinesApi
@Component
@Profile("!integration-test")
class CryptoPunksForSaleJob(
    private val properties: JobProperties,
    private val cryptoPunksClient: CryptoPunksClient,
    private val applicationEventPublisher: ApplicationEventPublisher,
    private val meterRegistry: MeterRegistry
) {

    private val logger = LoggerFactory.getLogger(CryptoPunksForSaleJob::class.java)

    @Scheduled(fixedDelay = DELAY_BETWEEN_INVOCATIONS)
    fun getPunkOffers() = runBlocking {
        meterRegistry.counter("offers.job.started").increment()
        try {
            supervisorScope {
                val chunks = (properties.startIndex..properties.endIndex).chunked(properties.batchSize)
                for (chunk in chunks) {
                    launch {
                        getOffers(chunk)
                    }
                }
            }
            meterRegistry.counter("offers.job.success").increment()
        } catch (e: Exception) {
            logger.error("Error getting punk offers", e)
            meterRegistry.counter("offers.job.failure").increment()
        }
    }

    private suspend fun getOffers(batch: List<Long>) {
        flow {
            for (punkIndex in batch) {
                val offer = cryptoPunksClient.getPunkOffer(BigInteger.valueOf(punkIndex))
                emit(offer)
            }
        }.flowOn(Dispatchers.IO)
            .collect { offer ->
                offer?.let {
                    applicationEventPublisher.publishEvent(OfferRetrievedEvent(this, it))
                }
            }
    }

    companion object {
        private const val DELAY_BETWEEN_INVOCATIONS = 60 * 5 * 1000L
    }
}

@ConfigurationProperties("job")
class JobProperties(
    val startIndex: Long,
    val endIndex: Long,
    val batchSize: Int
)
