package com.dankift.cryptopunks.importing

import com.dankift.cryptopunks.model.CryptoPunk
import com.dankift.cryptopunks.model.CryptoPunksRepository
import io.micrometer.core.instrument.MeterRegistry
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component

/*
 * Listens for OfferRetrievedEvents and saves new for sale statuses to database
 */
@Component
class OfferRetrievedListener(
    private val cryptoPunksRepository: CryptoPunksRepository,
    private val meterRegistry: MeterRegistry
) : ApplicationListener<OfferRetrievedEvent> {

    private val logger = LoggerFactory.getLogger(OfferRetrievedListener::class.java)

    override fun onApplicationEvent(event: OfferRetrievedEvent) {
        try {
            val offer = event.offer
            cryptoPunksRepository.findById(offer.punkIndex.toLong()).ifPresent { punk ->
                if (punk.forSale != offer.isForSale) {
                    updatePunkForSaleStatus(punk, offer)
                }
            }
        } catch (e: Exception) {
            meterRegistry.counter("offer.retrieved.listener.error").increment()
            logger.error("couldn't update punk with offer", e)
        }
    }

    private fun updatePunkForSaleStatus(
        punk: CryptoPunk,
        offer: Offer
    ) {
        logger.info("updating for sale status of punk with index ${offer.punkIndex}")
        meterRegistry.counter("punk.updated", "id", punk.id.toString()).increment()

        punk.forSale = offer.isForSale
        punk.forSalePriceEther = offer.minValue
        cryptoPunksRepository.save(punk)
    }
}
