package com.dankift.cryptopunks.importing

import org.springframework.context.ApplicationEvent

class OfferRetrievedEvent(source: Any, val offer: Offer) : ApplicationEvent(source)
