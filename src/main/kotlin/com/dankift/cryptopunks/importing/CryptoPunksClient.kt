package com.dankift.cryptopunks.importing

import io.micrometer.core.instrument.MeterRegistry
import java.math.BigInteger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.web3j.model.CryptoPunksMarket
import org.web3j.utils.Convert
import org.web3j.utils.Convert.Unit.ETHER

@Component
class CryptoPunksClient(
    private val cryptoPunksMarket: CryptoPunksMarket,
    private val meterRegistry: MeterRegistry
) {

    private val logger = LoggerFactory.getLogger(CryptoPunksClient::class.java)

    suspend fun getPunkOffer(id: BigInteger): Offer? {
        return try {
            cryptoPunksMarket.punksOfferedForSale(id).send().let {
                Offer(
                    isForSale = it.value1,
                    punkIndex = it.value2,
                    seller = it.value3,
                    minValue = Convert.fromWei(it.value4.toBigDecimal(), ETHER),
                    onlySellTo = it.value5
                )
            }
        } catch (e: Exception) {
            logger.error("error getting offer for punk $id", e)
            meterRegistry.counter("punk.offer.exception", "id", id.toString()).increment()
            null
        }
    }
}
