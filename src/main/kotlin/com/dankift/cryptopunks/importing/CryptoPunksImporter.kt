package com.dankift.cryptopunks.importing

import com.dankift.cryptopunks.model.Accessory
import com.dankift.cryptopunks.model.CryptoPunk
import com.dankift.cryptopunks.model.CryptoPunkGender
import com.dankift.cryptopunks.model.CryptoPunksRepository
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import javax.annotation.PostConstruct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

/*
 * Imports punks with gender and attributes from json file
 */
@Component
@Profile("!integration-test")
class CryptoPunksImporter(
    @Value("\${import.file}") private val fileName: String,
    private val cryptoPunksRepository: CryptoPunksRepository
) {

    private val logger = LoggerFactory.getLogger(CryptoPunksImporter::class.java)

    @PostConstruct
    fun import() {
        if (!punksAlreadyImported()) {
            logger.info("importing cryptopunks started, it may take one or two minutes")
            runBlocking(Dispatchers.IO) {
                val idToAttributes = loadFromFile()
                idToAttributes.entries
                    .chunked(CHUNK_SIZE)
                    .map {
                        async { processChunk(it) }
                    }
                    .awaitAll()
            }
            logger.info("importing cryptopunks complete")
        }
    }

    private fun punksAlreadyImported() = cryptoPunksRepository.existsById(0)

    private fun loadFromFile(): Map<Long, CryptoPunkAttributes> {
        val punkFile = CryptoPunksImporter::class.java.classLoader.getResource(fileName)
            ?: throw RuntimeException("No such file")
        return jacksonObjectMapper().readValue(punkFile)
    }

    private suspend fun processChunk(chunk: List<Map.Entry<Long, CryptoPunkAttributes>>) {
        val punks = chunk.map {
            it.toCryptoPunk()
        }
        cryptoPunksRepository.saveAll(punks)
    }

    private fun Map.Entry<Long, CryptoPunkAttributes>.toCryptoPunk(): CryptoPunk =
        CryptoPunk(
            id = this.key,
            forSale = false,
            gender = CryptoPunkGender.valueOf(this.value.gender.toUpperCase()),
            accessories = this.value.accessories.map { Accessory(name = it) }.toSet()
        )

    companion object {
        private const val CHUNK_SIZE = 50
    }
}

private data class CryptoPunkAttributes(
    val gender: String,
    val accessories: Collection<String>
)
