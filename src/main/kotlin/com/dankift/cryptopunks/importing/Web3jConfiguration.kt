package com.dankift.cryptopunks.importing

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.math.BigInteger
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.web3j.crypto.Credentials
import org.web3j.crypto.Wallet
import org.web3j.crypto.WalletFile
import org.web3j.model.CryptoPunksMarket
import org.web3j.protocol.Web3j
import org.web3j.protocol.http.HttpService
import org.web3j.tx.gas.StaticGasProvider

@ConfigurationProperties(prefix = "web3j")
class Web3jConfigurationProperties(
    val clientAddress: String,
    val contractAddress: String,
    val walletFile: String,
    val walletFilePassword: String
)

@Configuration
class Web3jConfiguration(
    private val properties: Web3jConfigurationProperties,
    private val objectMapper: ObjectMapper
) {

    @Bean
    fun web3j(): Web3j = Web3j.build(HttpService(properties.clientAddress))

    @Bean
    fun cryptoPunksMarket(web3j: Web3j): CryptoPunksMarket {
        val credentials = loadCredentials(properties.walletFile, properties.walletFilePassword)
        return CryptoPunksMarket.load(
            properties.contractAddress,
            web3j,
            credentials,
            StaticGasProvider(BigInteger.ZERO, BigInteger.ZERO)
        )
    }

    private fun loadCredentials(walletFileLocation: String, password: String): Credentials {
        val inputStream = Web3jConfiguration::class.java.classLoader.getResourceAsStream(walletFileLocation)
            ?: throw RuntimeException("no wallet file found")

        val walletFile = objectMapper.readValue<WalletFile>(inputStream)
        return Credentials.create(Wallet.decrypt(password, walletFile))
    }
}
