package com.dankift.cryptopunks.importing

import java.math.BigDecimal
import java.math.BigInteger

data class Offer(
    val isForSale: Boolean,
    val punkIndex: BigInteger,
    val seller: String,
    val minValue: BigDecimal,
    val onlySellTo: String
)
