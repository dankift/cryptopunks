package com.dankift.cryptopunks

import com.dankift.cryptopunks.importing.JobProperties
import com.dankift.cryptopunks.importing.Web3jConfigurationProperties
import com.dankift.cryptopunks.service.ValidationProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.hateoas.config.EnableHypermediaSupport
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
@EnableHypermediaSupport(type = [EnableHypermediaSupport.HypermediaType.HAL])
@EnableConfigurationProperties(ValidationProperties::class, Web3jConfigurationProperties::class, JobProperties::class)
class CryptoPunksApplication

fun main(args: Array<String>) {
    runApplication<CryptoPunksApplication>(*args)
}
