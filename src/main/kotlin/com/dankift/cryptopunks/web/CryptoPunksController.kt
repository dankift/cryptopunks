package com.dankift.cryptopunks.web

import com.dankift.cryptopunks.model.CryptoPunk
import com.dankift.cryptopunks.service.CryptoPunkException
import com.dankift.cryptopunks.service.CryptoPunksService
import io.micrometer.core.instrument.MeterRegistry
import java.time.ZoneId
import java.time.ZonedDateTime
import javax.servlet.http.HttpServletRequest
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.PagedModel
import org.springframework.http.HttpEntity
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/cryptopunks")
class CryptoPunksController(
    private val cryptoPunksService: CryptoPunksService,
    private val cryptoPunkModelAssembler: CryptoPunkModelAssembler,
    private val meterRegistry: MeterRegistry
) {

    private val logger = LoggerFactory.getLogger(CryptoPunksController::class.java)

    @GetMapping("/{id}", produces = ["application/json", "application/hal+json"])
    suspend fun getPunk(@PathVariable("id") id: Long): HttpEntity<EntityModel<CryptoPunk>> {
        val punk = cryptoPunksService.getPunk(id)
        val punkWithHypermedia = cryptoPunkModelAssembler.toModel(punk)
        return ResponseEntity.ok(punkWithHypermedia)
    }

    @GetMapping(produces = ["application/json", "application/hal+json"])
    suspend fun getPunksForSale(
        @PageableDefault(sort = ["id"], value = 20) pageable: Pageable,
        pagedResourcesAssembler: PagedResourcesAssembler<CryptoPunk>
    ): HttpEntity<PagedModel<EntityModel<CryptoPunk>>> {
        val punksPage = cryptoPunksService.getPunksForSale(pageable)
        val punksPageWithHypermedia = pagedResourcesAssembler.toModel(punksPage)
        return ResponseEntity.ok(punksPageWithHypermedia)
    }

    @ExceptionHandler
    fun handleCryptoPunkException(
        request: HttpServletRequest,
        cryptoPunkException: CryptoPunkException
    ): HttpEntity<ErrorResponse> {
        logger.error("Exception handling request for ${request.requestURI}", cryptoPunkException)
        meterRegistry.counter("exception", "exception", cryptoPunkException.toString(), "uri", request.requestURI)
            .increment()

        return ResponseEntity.status(cryptoPunkException.status())
            .body(
                ErrorResponse(
                    timestamp = ZonedDateTime.now(ZoneId.of("UTC")),
                    status = cryptoPunkException.status().value(),
                    message = cryptoPunkException.message(),
                    error = cryptoPunkException.status().reasonPhrase,
                    path = request.requestURI
                )
            )
    }
}
