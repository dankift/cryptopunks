package com.dankift.cryptopunks.web

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.ZonedDateTime

data class ErrorResponse(
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    val timestamp: ZonedDateTime,
    val status: Int,
    val error: String,
    val message: String,
    val path: String
)
