package com.dankift.cryptopunks.web

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class DocumentationRedirectController {

    @GetMapping
    fun redirect() = "redirect:/docs/index.html"
}
