package com.dankift.cryptopunks.web

import com.dankift.cryptopunks.model.CryptoPunk
import java.lang.reflect.Method
import kotlin.coroutines.Continuation
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.CollectionModel
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.stereotype.Component

@Component
class CryptoPunkModelAssembler : SimpleRepresentationModelAssembler<CryptoPunk> {
    override fun addLinks(resource: EntityModel<CryptoPunk>) {
        val id = resource.content?.id ?: throw RuntimeException("building link on resource without id")
        resource.add(linkTo(GET_PUNK_METHOD, id).withSelfRel())
        resource.add(linkTo(GET_PUNKS_METHOD, PageRequest.of(0, 20)).withRel("all"))
    }

    override fun addLinks(resources: CollectionModel<EntityModel<CryptoPunk>>) { /* not required */
    }

    companion object {

        val GET_PUNK_METHOD: Method = CryptoPunksController::class.java.getMethod(
            "getPunk",
            Long::class.java,
            Continuation::class.java
        )

        val GET_PUNKS_METHOD: Method = CryptoPunksController::class.java.getMethod(
            "getPunksForSale",
            Pageable::class.java,
            PagedResourcesAssembler::class.java,
            Continuation::class.java
        )
    }
}
